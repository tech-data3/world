# classicmodels

## Consigne

Télécharger et importer : [mysql sample database](https://www.mysqltutorial.org/getting-started-with-mysql/mysql-sample-database/).

### Modification du modèle.

Faire une version 2 du modèle de donnée. On veut apporter les changements suivants:
- [X] Utiliser classicmodel.customer.country pour stocker la clef du pays venant de la table world.country
- [X] Idem pour classicmodel.offices.country.
- [X] Remplacer le type du champ classicmodel.orders.status par un enum de tous les états différents dans la base de données.
- [X] Ajouter un champ classicmodel.customer.language qui devra contenir le code de la langue avec laquelle s’adresser au client en ISO 639-1.
- [X] Écrire le code SQL permettant de faire ces modifications dans un fichier migration.sql. 

### Migration de la base.

Pour appliquer notre migration on doit:

- [X] Créer une nouvelle BDD sur le même modèle que la première (sans les données).
- [X] Appliquer la migration (changer le modèle).
- [X] Importer les données depuis la première base de données en appliquant les modifications nécessaires (par exemple remplacer le nom du pays par la PK de world.country).

> pour la langue des clients, on mettra la première (la plus courante) langue officielle du pays.
## MLDs

### Version 1
```mermaid
erDiagram
customers }o--o| employees : salesRepEmployeeNumber
customers{
    customerNumber int PK "NOT NULL"
    customerName varchar(50) "NOT NULL"
    contactLastName varchar(50) "NOT NULL"
    contactFirstName varchar(50) "NOT NULL"
    phone varchar(50) "NOT NULL"
    addressLine1 varchar(50) "NOT NULL"
    addressLine2 varchar(50) "DEFAULT NULL"
    city varchar(50) "NOT NULL"
    state varchar(50) "DEFAULT NULL"
    postalCode varchar(15) "DEFAULT NULL"
    country varchar(50) "NOT NULL"
    salesRepEmployeeNumber int FK "DEFAULT NULL"
    creditLimit decimal "DEFAULT NULL"
}

productlines{
    productLine varchar(50) PK "NOT NULL"
    textDescription varchar(4000) "DEFAULT NULL"
    htmlDescription mediumtext
    image mediumblob
}
products{
    productCode varchar(15) PK "NOT NULL"
    productName varchar(70) "NOT NULL"
    productLine varchar(50) FK "NOT NULL"
    productScale varchar(10) "NOT NULL"
    productVendor varchar(50) "NOT NULL"
    productDescription text "NOT NULL"
    quantityInStock smallint "NOT NULL"
    buyPrice decimal "NOT NULL"
    MSRP decimal "NOT NULL"
}
orderdetails }|--|| orders : orderNumber
orderdetails }o--|| products : productCode
orderdetails {
    orderNumber int PK "NOT NULL"
    productCode varchar(15) PK, FK "NOT NULL"
    quantityOrdered int "NOT NULL"
    priceEach decimal "NOT NULL"
    orderLineNumber smallint FK "NOT NULL"
}
orders }o--|| customers : customerNumber
orders{
    orderNumber int PK "NOT NULL"
    orderDate date "NOT NULL"
    requiredDate date "NOT NULL"
    shippedDate date "DEFAULT NULL"
    status varchar(15) "NOT NULL"
    comments text
    customerNumber int FK "NOT NULL"
}
payments }o--|| customers : customerNumber
payments {
    customerNumber int PK, FK "NOT NULL"
    checkNumber varchar(50) PK "NOT NULL"
    paymentDate date "NOT NULL"
    amount decimal "NOT NULL"
}
employees |o--o{ employees : employeeNumber
employees {
    employeeNumber int PK "NOT NULL"
    lastName varchar(50) "NOT NULL"
    firstName varchar(50) "NOT NULL"
    extension varchar(10) "NOT NULL"
    email varchar(100) "NOT NULL"
    officeCode varchar(10) FK "NOT NULL"
    reportsTo int FK "DEFAULT NULL"
    jobTitle varchar(50) "NOT NULL"
}

offices ||--o{ employees : officeCode
offices {
    officeCode varchar(10) PK "NOT NULL"
    city varchar(50) "NOT NULL"
    phone varchar(50) "NOT NULL"
    addressLine1 varchar(50) "NOT NULL"
    addressLine2 varchar(50) "DEFAULT NULL"
    state varchar(50) "DEFAULT NULL"
    country varchar(50) "NOT NULL"
    postalCode varchar(15) "NOT NULL"
    territory varchar(10) "NOT NULL"
}

productlines ||--o{ products : productline
```
### Version 2
```mermaid
erDiagram
customers }o--o| employees : salesRepEmployeeNumber
customers{
    customerNumber int PK "NOT NULL"
    customerName varchar(50) "NOT NULL"
    contactLastName varchar(50) "NOT NULL"
    contactFirstName varchar(50) "NOT NULL"
    phone varchar(50) "NOT NULL"
    addressLine1 varchar(50) "NOT NULL"
    addressLine2 varchar(50) "DEFAULT NULL"
    city varchar(50) "NOT NULL"
    state varchar(50) "DEFAULT NULL"
    postalCode varchar(15) "DEFAULT NULL"
    country char(3) "NOT NULL"
    salesRepEmployeeNumber int FK "DEFAULT NULL"
    creditLimit decimal "DEFAULT NULL"
    language char(2) "DEFAULT NULL"
}

productlines{
    productLine varchar(50) PK "NOT NULL"
    textDescription varchar(4000) "DEFAULT NULL"
    htmlDescription mediumtext
    image mediumblob
}
products{
    productCode varchar(15) PK "NOT NULL"
    productName varchar(70) "NOT NULL"
    productLine varchar(50) FK "NOT NULL"
    productScale varchar(10) "NOT NULL"
    productVendor varchar(50) "NOT NULL"
    productDescription text "NOT NULL"
    quantityInStock smallint "NOT NULL"
    buyPrice decimal "NOT NULL"
    MSRP decimal "NOT NULL"
}
orderdetails }|--|| orders : orderNumber
orderdetails }o--|| products : productCode
orderdetails {
    orderNumber int PK "NOT NULL"
    productCode varchar(15) PK, FK "NOT NULL"
    quantityOrdered int "NOT NULL"
    priceEach decimal "NOT NULL"
    orderLineNumber smallint FK "NOT NULL"
}
orders }o--|| customers : customerNumber
orders{
    orderNumber int PK "NOT NULL"
    orderDate date "NOT NULL"
    requiredDate date "NOT NULL"
    shippedDate date "DEFAULT NULL"
    status enum "NOT NULL"
    comments text
    customerNumber int FK "NOT NULL"
}
payments }o--|| customers : customerNumber
payments {
    customerNumber int PK, FK "NOT NULL"
    checkNumber varchar(50) PK "NOT NULL"
    paymentDate date "NOT NULL"
    amount decimal "NOT NULL"
}
employees |o--o{ employees : employeeNumber
employees {
    employeeNumber int PK "NOT NULL"
    lastName varchar(50) "NOT NULL"
    firstName varchar(50) "NOT NULL"
    extension varchar(10) "NOT NULL"
    email varchar(100) "NOT NULL"
    officeCode varchar(10) FK "NOT NULL"
    reportsTo int FK "DEFAULT NULL"
    jobTitle varchar(50) "NOT NULL"
}

offices ||--o{ employees : officeCode
offices {
    officeCode varchar(10) PK "NOT NULL"
    city varchar(50) "NOT NULL"
    phone varchar(50) "NOT NULL"
    addressLine1 varchar(50) "NOT NULL"
    addressLine2 varchar(50) "DEFAULT NULL"
    state varchar(50) "DEFAULT NULL"
    country char(3) "NOT NULL"
    postalCode varchar(15) "NOT NULL"
    territory varchar(10) "NOT NULL"
}

productlines ||--o{ products : productline
```
## Questions.

### Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.

#### Requetes

```sql
SELECT
    country,
    count(distinct c.customerNumber) as `nombre de client`,
    language AS `langue`,
    sum(od.priceEach*quantityOrdered) as `CA`
from customers AS c
JOIN orders as o ON c.customerNumber = o.customerNumber
JOIN orderdetails AS od ON od.orderNumber = o.orderNumber
GROUP BY country, language;
```

#### Resultat

| country | nombre de client | langue |         CA |
|:--------|-----------------:|:-------|-----------:|
| AUS     |                5 | en     |  562582.59 |
| AUT     |                2 | de     |  188540.06 |
| BEL     |                2 | nl     |  100068.76 |
| CAN     |                3 | en     |  205911.86 |
| CHE     |                1 | de     |  108777.92 |
| DEU     |                3 | de     |  196470.99 |
| DNK     |                2 | da     |  218994.92 |
| ESP     |                5 | es     | 1099389.09 |
| FIN     |                3 | fi     |  295149.35 |
| FRA     |               12 | fr     | 1007374.02 |
| GBR     |                5 | en     |  436947.44 |
| HKG     |                1 | en     |   45480.79 |
| IRL     |                1 | en     |   49898.27 |
| ITA     |                4 | it     |  360616.81 |
| JPN     |                2 | ja     |  167909.95 |
| NOR     |                3 | no     |  270846.30 |
| NZL     |                4 | en     |  476847.01 |
| PHL     |                1 | NULL   |   87468.30 |
| SGP     |                2 | zh     |  263997.78 |
| SWE     |                2 | sv     |  187638.35 |
| USA     |               35 | en     | 3273280.05 |


### La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.

#### Requetes

```sql
SELECT 
    c.customerName, 
    sum(od.priceEach*quantityOrdered) as `CA`
FROM customers AS c
JOIN orders AS o on c.customerNumber = o.customerNumber
JOIN orderdetails AS od on od.orderNumber = o.orderNumber
GROUP BY c.customerName
ORDER BY `CA` desc 
LIMIT 10;
```

#### Resultat

| customerName                 | CA        |
|:-----------------------------|----------:|
| Euro+ Shopping Channel       | 820689.54 |
| Mini Gifts Distributors Ltd. | 591827.34 |
| Australian Collectors, Co.   | 180585.07 |
| Muscle Machine Inc           | 177913.95 |
| La Rochelle Gifts            | 158573.12 |
| Dragon Souveniers, Ltd.      | 156251.03 |
| Down Under Souveniers, Inc   | 154622.08 |
| Land of Toys Inc.            | 149085.15 |
| AV Stores, Co.               | 148410.09 |
| The Sharp Gifts Warehouse    | 143536.27 |


### La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).

#### Requetes

```sql
SELECT avg(datediff(shippedDate,orderDate)) as `durée moyenne en jours entre les dates de commande et d'expédition`
FROM orders;
```

#### Resultat

| durée moyenne en jours entre les dates de commande et d'expédition   |
|---------------------------------------------------------------------:|
|                                                               3.7564 |

### Les 10 produits les plus vendus.

#### Requetes

```sql
SELECT p.productName, sum(pl.quantityOrdered) AS qt
FROM orderdetails as pl
JOIN products as p on p.productCode = pl.productCode
GROUP BY pl.productCode
ORDER BY qt DESC
LIMIT 10;
```
#### Resultat

| productName                             | qt   |
|:----------------------------------------|-----:|
| 1992 Ferrari 360 Spider red             | 1808 |
| 1937 Lincoln Berline                    | 1111 |
| American Airlines: MD-11S               | 1085 |
| 1941 Chevrolet Special Deluxe Cabriolet | 1076 |
| 1930 Buick Marquette Phaeton            | 1074 |
| 1940s Ford truck                        | 1061 |
| 1969 Harley Davidson Ultimate Chopper   | 1057 |
| 1957 Chevy Pickup                       | 1056 |
| 1964 Mercedes Tour Bus                  | 1053 |
| 1956 Porsche 356A Coupe                 | 1052 |

### Pour chaque pays le produits le plus vendu.

#### Requetes

```sql
select s.country, s.productName, s.qt
FROM (
    SELECT 
        c.country, 
        p.productName, 
        SUM(pl.quantityOrdered) AS qt,
        DENSE_RANK() OVER (PARTITION BY c.country ORDER BY SUM(pl.quantityOrdered) DESC) AS rn
    FROM orderdetails AS pl
    JOIN orders AS o ON o.orderNumber = pl.orderNumber
    JOIN customers AS c ON c.customerNumber = o.customerNumber
    JOIN products AS p ON p.productCode = pl.productCode
    GROUP BY 
        c.country, 
        p.productCode
) AS s WHERE s.rn = 1;
```

#### Resultat

| country | productName                                 |   qt |
|:--------|:--------------------------------------------|-----:|
| AUS     | 1913 Ford Model T Speedster                 |  231 |
| AUT     | 1969 Dodge Charger                          |  128 |
| BEL     | 1941 Chevrolet Special Deluxe Cabriolet     |   95 |
| CAN     | 1996 Peterbilt 379 Stake Bed with Outrigger |   97 |
| CAN     | 1982 Camaro Z28                             |   97 |
| CHE     | 1948 Porsche 356-A Roadster                 |   91 |
| DEU     | 1971 Alpine Renault 1600s                   |  117 |
| DNK     | 1948 Porsche Type 356 Roadster              |  101 |
| ESP     | 1992 Ferrari 360 Spider red                 |  336 |
| FIN     | 1992 Ferrari 360 Spider red                 |  141 |
| FRA     | 2002 Yamaha YZR M1                          |  278 |
| GBR     | 1952 Citroen-15CV                           |  162 |
| HKG     | 1928 British Royal Navy Airplane            |   79 |
| IRL     | 1970 Triumph Spitfire                       |   50 |
| ITA     | 1928 British Royal Navy Airplane            |  162 |
| JPN     | American Airlines: MD-11S                   |   92 |
| NOR     | 1969 Harley Davidson Ultimate Chopper       |   89 |
| NZL     | 1969 Dodge Super Bee                        |  142 |
| PHL     | 1957 Corvette Convertible                   |   94 |
| SGP     | 1940s Ford truck                            |  128 |
| SWE     | 1940s Ford truck                            |   97 |
| USA     | 1957 Chevy Pickup                           |  523 |

### Le produit qui a rapporté le plus de bénéfice.

#### Requetes

```sql
SELECT 
    p.productName, 
    SUM((od.priceEach - p.buyPrice)*od.quantityOrdered) AS benef
FROM orderdetails AS od
JOIN products AS p ON p.productCode = od.productCode
GROUP BY p.productCode
ORDER BY benef DESC 
LIMIT 1;
```

#### Resultat

| productName                 | benef     |
|:----------------------------|----------:|
| 1992 Ferrari 360 Spider red | 135996.78 |

### La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).

#### Requetes

```sql
SELECT p.productName, avg(p.MSRP - od.priceEach) AS avgdif
FROM products AS p
JOIN orderdetails AS od ON p.productCode = od.productCode
GROUP BY p.productCode 
ORDER BY avgdif DESC
LIMIT 10;
```

#### Resultat

| productName                           | avgdif    |
|:--------------------------------------|----------:|
| 1968 Ford Mustang                     | 22.123704 |
| 2003 Harley-Davidson Eagle Drag Bike  | 21.371786 |
| 2001 Ferrari Enzo                     | 20.703333 |
| 1993 Mazda RX-7                       | 20.234444 |
| 1998 Chrysler Plymouth Prowler        | 19.587143 |
| 1957 Corvette Convertible             | 19.509259 |
| 1928 Mercedes-Benz SSK                | 18.862500 |
| 1980s Black Hawk Helicopter           | 18.584643 |
| 2002 Suzuki XREO                      | 18.451071 |
| 1992 Ferrari 360 Spider red           | 16.997170 |


### Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.

#### Requetes

```sql
SELECT o.country, COUNT(*) AS `nombre de bureaux`
FROM offices AS o
WHERE (
    SELECT COUNT(*)
    FROM customers AS cc
    WHERE cc.country = o.country
) > 1
GROUP BY o.country;
```
#### Resultat

| country | nombre de bureaux |
|:--------|------------------:|
| USA     |                 3 |
| FRA     |                 1 |
| JPN     |                 1 |
| AUS     |                 1 |
| GBR     |                 1 |
