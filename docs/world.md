# world_db

## Analyse bdd

### Traversal a toute les tables

#### MLD
![mld.png](mld.png)

#### pourquoi char(n) ?

char(n) est de taille fixe, si la longueur du text est plus petite que n il sera stocke comme si 
il etait de longueur n.

##### NULL avec default

Si l'on envoie NULL mysql retourne une erreur, si 
on envoie rien mysql utilise la valeur par defaut

```sql
# PAS BIEN
INSERT INTO table(field_with_default_not_null,other field) VALUES (NULL,"chepa");

# BIEN
INSERT INTO table(other_field) VALUES ("chepa");
```

### city

#### sql :

```sql
CREATE TABLE `city` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Name` char(35) NOT NULL DEFAULT '',
  `CountryCode` char(3) NOT NULL DEFAULT '',
  `District` char(20) NOT NULL DEFAULT '',
  `Population` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CountryCode` (`CountryCode`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`CountryCode`) REFERENCES `country` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=4080 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

#### champs de la table

##### ID

Type: int, Null: NO, Key: PRI, Extra: auto_increment, 

##### Name

Type: char(35), Null: NO, 

nom de la ville.

##### CountryCode

Type: char(3), Null: NO, Key: MUL,

code pays sur 3 lettre :
ISO 3166-1 alpha-3
https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3

##### District

Type: char(20), Null: NO, 

nom de la province.

##### Population

Type: int, Null: NO, Default: 0, 

la population

#### Contraintes

```sql
KEY `CountryCode` (`CountryCode`),
CONSTRAINT `city_ibfk_1` FOREIGN KEY (`CountryCode`) REFERENCES `country` (`Code`)
```

le code pays (identifiant a 3 lettre) sert de clef etrangere qui reference la table country.

### country

#### sql :

```sql
CREATE TABLE `country` (
  `Code` char(3) NOT NULL DEFAULT '',
  `Name` char(52) NOT NULL DEFAULT '',
  `Continent` enum('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL DEFAULT 'Asia',
  `Region` char(26) NOT NULL DEFAULT '',
  `SurfaceArea` decimal(10,2) NOT NULL DEFAULT '0.00',
  `IndepYear` smallint DEFAULT NULL,
  `Population` int NOT NULL DEFAULT '0',
  `LifeExpectancy` decimal(3,1) DEFAULT NULL,
  `GNP` decimal(10,2) DEFAULT NULL,
  `GNPOld` decimal(10,2) DEFAULT NULL,
  `LocalName` char(45) NOT NULL DEFAULT '',
  `GovernmentForm` char(45) NOT NULL DEFAULT '',
  `HeadOfState` char(60) DEFAULT NULL,
  `Capital` int DEFAULT NULL,
  `Code2` char(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

#### champs de la table

##### Code

Type: char(3), Null: NO, Key: PRI, 

Cf. city.CountryCode

##### Name

Type: char(52), Null: NO, 

Nom du pays commencant par une majuscule

##### Continent

Type: enum('Asia','Europe','North America','Africa','Oceania','Antarctica','South America'), Null: NO, Default: Asia, 

Continent sur lequel se situe le pays.
majuscule a tout les debut de mot.

##### Region

Type: char(26), Null: NO, 

Region du monde,
Majuscule a tout les debut de mot

##### SurfaceArea

Type: decimal(10,2), Null: NO, Default: 0.00, 

Surface en km**2, sur 10 chiffres, dont 2 apres la
virgule.

##### IndepYear

Type: smallint, Null: YES, 

Annee d'independance. si elle existe.

##### Population

Type: int, Null: NO, Default: 0, 

population

##### LifeExpectancy

Type: decimal(3,1), Null: YES, 

esperence de vie

##### GNP

Type: decimal(10,2), Null: YES, 

produit national brut

##### GNPOld

Type: decimal(10,2), Null: YES, 

produit national brut annee n-1 ? 2002-2003

##### LocalName

Type: char(45), Null: NO, 

##### GovernmentForm

Type: char(45), Null: NO, 

type de gouvernement

##### HeadOfState

Type: char(60), Null: YES, 

Chef de l'etat

##### Capital

Type: int, Null: YES, 

id de la ville etant la capital, c'est en quelque sorte une foreign key

##### Code2

Type: char(2), Null: NO, 

code de pays different


### countrylanguage

#### sql :

```sql
CREATE TABLE `countrylanguage` (
  `CountryCode` char(3) NOT NULL DEFAULT '',
  `Language` char(30) NOT NULL DEFAULT '',
  `IsOfficial` enum('T','F') NOT NULL DEFAULT 'F',
  `Percentage` decimal(4,1) NOT NULL DEFAULT '0.0',
  PRIMARY KEY (`CountryCode`,`Language`),
  KEY `CountryCode` (`CountryCode`),
  CONSTRAINT `countryLanguage_ibfk_1` FOREIGN KEY (`CountryCode`) REFERENCES `country` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

#### champs de la table

##### CountryCode

Type: char(3), Null: NO, Key: PRI, 

Cf countrycode

##### Language

Type: char(30), Null: NO, Key: PRI, 

Langue parlee

##### IsOfficial

Type: enum('T','F'), Null: NO, Default: F, 

langue officiel ? Pas booleen

##### Percentage

Type: decimal(4,1), Null: NO, Default: 0.0, 

Pourcentage de la population parlant la langue

## Questions

### Liste des type de gouvernement avec nombre de pays pour chaque.

#### Requete

```mysql
SELECT 
    GovernmentForm AS `type de gouvernement`, 
    COUNT(*) AS `nombre de pays` 
FROM country 
GROUP BY GovernmentForm 
ORDER BY `nombre de pays` DESC;
```

#### Resultat

| type de gouvernement                         | nombre de pays |
|:---------------------------------------------|---------------:|
| Republic                                     |            123 |
| Constitutional Monarchy                      |             29 |
| Federal Republic                             |             14 |
| Dependent Territory of the UK                |             12 |
| Monarchy                                     |              5 |
| Overseas Department of France                |              4 |
| Nonmetropolitan Territory of France          |              4 |
| Constitutional Monarchy, Federation          |              4 |
| Territory of Australia                       |              4 |
| Socialistic Republic                         |              3 |
| Nonmetropolitan Territory of New Zealand     |              3 |
| US Territory                                 |              3 |
| Commonwealth of the US                       |              2 |
| Territorial Collectivity of France           |              2 |
| Nonmetropolitan Territory of The Netherlands |              2 |
| Dependent Territory of Norway                |              2 |
| Monarchy (Sultanate)                         |              2 |
| Part of Denmark                              |              2 |
| Special Administrative Region of China       |              2 |
| Islamic Republic                             |              2 |
| Federation                                   |              1 |
| Socialistic State                            |              1 |
| Autonomous Area                              |              1 |
| Administrated by the UN                      |              1 |
| Dependent Territory of the US                |              1 |
| Independent Church State                     |              1 |
| Parlementary Monarchy                        |              1 |
| Constitutional Monarchy (Emirate)            |              1 |
| Occupied by Marocco                          |              1 |
| People'sRepublic                             |              1 |
| Monarchy (Emirate)                           |              1 |
| Co-administrated                             |              1 |
| Emirate Federation                           |              1 |
| Parliamentary Coprincipality                 |              1 |
| Islamic Emirate                              |              1 |


### Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?
### D’apres la BDD, combien de personne dans le monde parle anglais ?

#### requete

```sql
SELECT 
    SUM(country.Population * cl.Percentage / 100) AS `population mondiale parlant anglais` 
FROM countrylanguage AS cl 
JOIN country ON country.Code = cl.CountryCode 
WHERE cl.Language LIKE "english";
```

#### Resultat

|   population mondiale parlant anglais |
|--------------------------------------:|
|                       347077867.30000 |

### Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.

#### requete

```sql
SELECT cl.Language, SUM(country.Population * cl.Percentage / 100) AS `nb locuteur` 
FROM countrylanguage AS cl 
JOIN country ON country.Code = cl.CountryCode 
GROUP BY cl.Language 
ORDER BY `nb locuteur` DESC
LIMIT 50;
```

#### resultat
| Language       | nb locuteur      |
|:---------------|-----------------:|
| Chinese        | 1191843539.00000 |
| Hindi          |  405633070.00000 |
| Spanish        |  355029462.00000 |
| English        |  347077867.30000 |
| Arabic         |  233839238.70000 |
| Bengali        |  209304719.00000 |
| Portuguese     |  177595269.40000 |
| Russian        |  160807561.30000 |
| Japanese       |  126814108.00000 |
| Punjabi        |  104025371.00000 |
| German         |   92133584.70000 |
| Javanese       |   83570158.00000 |
| Telugu         |   79065636.00000 |
| Marathi        |   75019094.00000 |
| Korean         |   72291372.00000 |
| Vietnamese     |   70616218.00000 |
| French         |   69980880.40000 |
| Tamil          |   68691536.00000 |
| Urdu           |   63589470.00000 |
| Turkish        |   62205657.20000 |
| Italian        |   59864483.20000 |
| Gujarati       |   48655776.00000 |
| Malay          |   41517994.00000 |
| Kannada        |   39532818.00000 |
| Polish         |   39525035.10000 |
| Ukrainian      |   36593408.00000 |
| Malajalam      |   36491832.00000 |
| Thai           |   33996960.00000 |
| Sunda          |   33512906.00000 |
| Orija          |   33450846.00000 |
| Pashto         |   32404553.00000 |
| Burmese        |   31471590.00000 |
| Persian        |   31124734.00000 |
| Hausa          |   29225396.00000 |
| Joruba         |   24868874.00000 |
| Ful            |   23704612.00000 |
| Romanian       |   23457777.30000 |
| Uzbek          |   22535760.00000 |
| Pilipino       |   22258331.00000 |
| Dutch          |   21388666.00000 |
| Ibo            |   20182586.00000 |
| Lao            |   20167307.00000 |
| Oromo          |   19395150.00000 |
| Kurdish        |   19062628.00000 |
| Azerbaijani    |   19014911.00000 |
| Amhara         |   18769500.00000 |
| Sindhi         |   18464994.00000 |
| Zhuang         |   17885812.00000 |
| Cebuano        |   17700311.00000 |
| Serbo-Croatian |   16762504.70000 |

### En quelle unité est exprimée la surface des pays ?

#### Reponse

cf. definition des champs de la table country

### Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.

#### requete

```sql
SELECT 
    country.Name, 
    city.Name, 
    city.Population*100/country.Population AS `pourcentage de la population du pays habitant la capitale` 
FROM country 
JOIN city ON city.ID = country.Capital 
WHERE country.Population > 10000000 
LIMIT 50;
```
#### Resultat

| Name                                  | Name                | pourcentage de la population du pays habitant la capitale |
|:--------------------------------------|:--------------------|----------------------------------------------------------:|
| Afghanistan                           | Kabul               |                                                    7.8345 |
| Angola                                | Luanda              |                                                   15.7012 |
| Argentina                             | Buenos Aires        |                                                    8.0529 |
| Australia                             | Canberra            |                                                    1.7088 |
| Belgium                               | Bruxelles [Brussel] |                                                    1.3073 |
| Burkina Faso                          | Ouagadougou         |                                                    6.9029 |
| Bangladesh                            | Dhaka               |                                                    2.7973 |
| Belarus                               | Minsk               |                                                   16.3540 |
| Brazil                                | Brasília            |                                                    1.1580 |
| Canada                                | Ottawa              |                                                    1.0764 |
| Chile                                 | Santiago de Chile   |                                                   30.9247 |
| China                                 | Peking              |                                                    0.5849 |
| Côte d’Ivoire                         | Yamoussoukro        |                                                    0.8792 |
| Cameroon                              | Yaoundé             |                                                    9.1004 |
| Congo, The Democratic Republic of the | Kinshasa            |                                                    9.8037 |
| Colombia                              | Santafé de Bogotá   |                                                   14.7937 |
| Cuba                                  | La Habana           |                                                   20.1411 |
| Czech Republic                        | Praha               |                                                   11.4917 |
| Germany                               | Berlin              |                                                    4.1218 |
| Algeria                               | Alger               |                                                    6.8889 |
| Ecuador                               | Quito               |                                                   12.4423 |
| Egypt                                 | Cairo               |                                                    9.9160 |
| Spain                                 | Madrid              |                                                    7.2995 |
| Ethiopia                              | Addis Abeba         |                                                    3.9879 |
| France                                | Paris               |                                                    3.5884 |
| United Kingdom                        | London              |                                                   12.2184 |
| Ghana                                 | Accra               |                                                    5.2939 |
| Greece                                | Athenai             |                                                    7.3212 |
| Guatemala                             | Ciudad de Guatemala |                                                    7.2315 |
| Hungary                               | Budapest            |                                                   18.0376 |
| Indonesia                             | Jakarta             |                                                    4.5283 |
| India                                 | New Delhi           |                                                    0.0297 |
| Iran                                  | Teheran             |                                                    9.9832 |
| Iraq                                  | Baghdad             |                                                   18.7584 |
| Italy                                 | Roma                |                                                    4.5832 |
| Japan                                 | Tokyo               |                                                    6.2978 |
| Kazakstan                             | Astana              |                                                    1.9183 |
| Kenya                                 | Nairobi             |                                                    7.6130 |
| Cambodia                              | Phnom Penh          |                                                    5.1053 |
| South Korea                           | Seoul               |                                                   21.3082 |
| Sri Lanka                             | Colombo             |                                                    3.4259 |
| Morocco                               | Rabat               |                                                    2.1991 |
| Madagascar                            | Antananarivo        |                                                    4.2383 |
| Mexico                                | Ciudad de México    |                                                    8.6885 |
| Mali                                  | Bamako              |                                                    7.2063 |
| Myanmar                               | Rangoon (Yangon)    |                                                    7.3704 |
| Mozambique                            | Maputo              |                                                    5.1775 |
| Malawi                                | Lilongwe            |                                                    3.9905 |
| Malaysia                              | Kuala Lumpur        |                                                    5.8332 |
| Niger                                 | Niamey              |                                                    3.9143 |

### Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance

#### Requete

```sql
SELECT Name AS pays, ((gnp/gnpold)-1)*100 AS `Taux de croissance` 
FROM country 
ORDER BY `taux de croissance`DESC 
LIMIT 10;
```

#### Resultat
| pays                                  | Taux de croissance |
|:--------------------------------------|-------------------:|
| Congo, The Democratic Republic of the |         181.487470 |
| Turkmenistan                          |         119.850000 |
| Tajikistan                            |          88.446970 |
| Estonia                               |          58.053990 |
| Albania                               |          28.200000 |
| Suriname                              |          23.229462 |
| Iran                                  |          22.225899 |
| Bulgaria                              |          19.756122 |
| Honduras                              |          13.540558 |
| Latvia                                |          13.459833 |

### Liste des pays plurilingues avec pour chacun le nombre de langues parlées.

#### requete
```sql
SELECT country.Name, count(*) AS `nb langage` 
FROM countrylanguage AS cl 
JOIN country ON country.Code = cl.CountryCode 
GROUP BY country.name 
HAVING `nb langage` > 1 
ORDER BY `nb langage` DESC 
LIMIT 50;
```

#### resultat

| Name                                   | nb langage |
|:---------------------------------------|-----------:|
| Canada                                 |         12 |
| China                                  |         12 |
| India                                  |         12 |
| Russian Federation                     |         12 |
| United States                          |         12 |
| Tanzania                               |         11 |
| South Africa                           |         11 |
| Congo, The Democratic Republic of the  |         10 |
| Iran                                   |         10 |
| Kenya                                  |         10 |
| Mozambique                             |         10 |
| Nigeria                                |         10 |
| Philippines                            |         10 |
| Sudan                                  |         10 |
| Uganda                                 |         10 |
| Angola                                 |          9 |
| Indonesia                              |          9 |
| Vietnam                                |          9 |
| Australia                              |          8 |
| Austria                                |          8 |
| Cameroon                               |          8 |
| Czech Republic                         |          8 |
| Italy                                  |          8 |
| Liberia                                |          8 |
| Myanmar                                |          8 |
| Namibia                                |          8 |
| Pakistan                               |          8 |
| Sierra Leone                           |          8 |
| Chad                                   |          8 |
| Togo                                   |          8 |
| Benin                                  |          7 |
| Bangladesh                             |          7 |
| Denmark                                |          7 |
| Ethiopia                               |          7 |
| Guinea                                 |          7 |
| Kyrgyzstan                             |          7 |
| Nepal                                  |          7 |
| Ukraine                                |          7 |
| Belgium                                |          6 |
| Burkina Faso                           |          6 |
| Central African Republic               |          6 |
| Congo                                  |          6 |
| Germany                                |          6 |
| Eritrea                                |          6 |
| France                                 |          6 |
| Micronesia, Federated States of        |          6 |
| Georgia                                |          6 |
| Ghana                                  |          6 |
| Guinea-Bissau                          |          6 |
| Hungary                                |          6 |

### Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.

#### requete

```sql
SELECT 
    country.Name, 
    count(cl.Language) AS `nombre de langue parlee dans le pays`, 
    SUM(CASE WHEN cl.isOfficial = "T" THEN 1 ELSE 0 END) AS `nombre de langue officielles` 
FROM countrylanguage as cl 
JOIN country ON country.Code = cl.CountryCode 
GROUP BY country.Name 
HAVING `nombre de langue officielles` > 1 
ORDER BY `nombre de langue officielles` DESC;
```

#### Resultat

| Name                 | nombre de langue parlee dans le pays | nombre de langue officielles |
|:---------------------|-------------------------------------:|-----------------------------:|
| South Africa         |                                   11 |                            4 |
| Switzerland          |                                    4 |                            4 |
| Luxembourg           |                                    5 |                            3 |
| Vanuatu              |                                    3 |                            3 |
| Singapore            |                                    3 |                            3 |
| Peru                 |                                    3 |                            3 |
| Bolivia              |                                    4 |                            3 |
| Belgium              |                                    6 |                            3 |
| Somalia              |                                    2 |                            2 |
| Nauru                |                                    5 |                            2 |
| Palau                |                                    4 |                            2 |
| Paraguay             |                                    4 |                            2 |
| Romania              |                                    6 |                            2 |
| Rwanda               |                                    2 |                            2 |
| Burundi              |                                    3 |                            2 |
| Malta                |                                    2 |                            2 |
| Seychelles           |                                    3 |                            2 |
| Togo                 |                                    8 |                            2 |
| Tonga                |                                    2 |                            2 |
| Tuvalu               |                                    3 |                            2 |
| American Samoa       |                                    3 |                            2 |
| Samoa                |                                    3 |                            2 |
| Netherlands Antilles |                                    3 |                            2 |
| Marshall Islands     |                                    2 |                            2 |
| Madagascar           |                                    2 |                            2 |
| Afghanistan          |                                    5 |                            2 |
| Lesotho              |                                    3 |                            2 |
| Sri Lanka            |                                    3 |                            2 |
| Kyrgyzstan           |                                    7 |                            2 |
| Israel               |                                    3 |                            2 |
| Ireland              |                                    2 |                            2 |
| Guam                 |                                    5 |                            2 |
| Greenland            |                                    2 |                            2 |
| Faroe Islands        |                                    2 |                            2 |
| Finland              |                                    5 |                            2 |
| Cyprus               |                                    2 |                            2 |
| Belarus              |                                    4 |                            2 |
| Canada               |                                   12 |                            2 |

### Liste des langues parlées en France avec le % pour chacune.

#### Requete
```sql
SELECT 
    cl.Language AS `Langue parlee en France`, 
    cl.Percentage 
FROM country 
JOIN countrylanguage AS cl ON country.Code = cl.CountryCode 
WHERE country.Name LIKE "france" 
ORDER BY cl.Percentage DESC;
```

#### Resultat

| Langue parlee en France | Percentage |
|:------------------------|-----------:|
| French                  |       93.6 |
| Arabic                  |        2.5 |
| Portuguese              |        1.2 |
| Italian                 |        0.4 |
| Spanish                 |        0.4 |
| Turkish                 |        0.4 |

### Pareil en chine.

#### Requete

```sql
select 
    cl.Language AS `Langue parlee en Chine`, 
    cl.Percentage 
FROM country 
JOIN countrylanguage AS cl ON country.Code = cl.CountryCode 
WHERE country.Name like "china" 
ORDER BY cl.Percentage DESC;

```

#### Resultat

| Langue parlee en France  | Percentage |
|:-------------------------|-----------:|
| Chinese                  |       92.0 |
| Zhuang                   |        1.4 |
| Mantšu                   |        0.9 |
| Hui                      |        0.8 |
| Miao                     |        0.7 |
| Uighur                   |        0.6 |
| Yi                       |        0.6 |
| Tujia                    |        0.5 |
| Mongolian                |        0.4 |
| Tibetan                  |        0.4 |
| Dong                     |        0.2 |
| Puyi                     |        0.2 |

### Pareil aux états unis.

#### Requete

```sql
SELECT 
    cl.Language AS `Langue parlee en France`, 
    cl.Percentage 
FROM country 
JOIN countrylanguage AS cl ON country.Code = cl.CountryCode 
WHERE country.Code = "USA"  
ORDER BY cl.Percentage DESC;
```
#### Resultat

| Langue parlee aux etats unis | Percentage |
|:-----------------------------|-----------:|
| English                      |       86.2 |
| Spanish                      |        7.5 |
| French                       |        0.7 |
| German                       |        0.7 |
| Chinese                      |        0.6 |
| Italian                      |        0.6 |
| Tagalog                      |        0.4 |
| Korean                       |        0.3 |
| Polish                       |        0.3 |
| Japanese                     |        0.2 |
| Portuguese                   |        0.2 |
| Vietnamese                   |        0.2 |

### Pareil aux UK.

#### Requete

```sql
SELECT 
    cl.Language AS `Langue parlee en France`, 
    cl.Percentage 
FROM country 
JOIN countrylanguage AS cl ON country.Code = cl.CountryCode 
WHERE country.Code = "GBR"  
ORDER BY cl.Percentage DESC;
```
#### Resultat

| Langue parlee en grande bretagne | Percentage |
|:---------------------------------|-----------:|
| English                          |       97.3 |
| Kymri                            |        0.9 |
| Gaeli                            |        0.1 |

### Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle.
#### Requete
[le fichier](../scripts/test.sql)

#### Resultat
| Language         | Region                    | Pourcent |
|:-----------------|:--------------------------|---------:|
| Spanish          | Caribbean                 |  56.4523 |
| Hindi            | Southern and Central Asia |  27.1784 |
| Kongo            | Central Africa            |   12.002 |
| Italian          | Southern Europe           |  37.5353 |
| Arabic           | Middle East               |  47.3264 |
| Portuguese       | South America             |  48.0184 |
| Samoan           | Polynesia                 |   23.238 |
| English          | Australia and New Zealand |  82.1663 |
| German           | Western Europe            |  47.5619 |
| Oromo            | Eastern Africa            |  7.85232 |
| Hausa            | Western Africa            |  13.1841 |
| Russian          | Eastern Europe            |  48.2489 |
| Spanish          | Central America           |  90.3404 |
| English          | North America             |    83.59 |
| Javanese         | Southeast Asia            |  16.1164 |
| Zulu             | Southern Africa           |  20.2804 |
| Chinese          | Eastern Asia              |  77.9975 |
| Swedish          | Nordic Countries          |  34.1596 |
| Arabic           | Northern Africa           |  80.8491 |
| Lithuanian       | Baltic Countries          |   40.295 |
| Papuan Languages | Melanesia                 |  58.5978 |
| Kiribati         | Micronesia                |  15.5129 |
| English          | British Islands           |  97.3655 |

### Est-ce que la somme des pourcentage de langue parlée dans un pays est égale à 100 ? Pourquoi ?

#### Requete

```sql
SELECT country.name, SUM(cl.Percentage) AS `somme des pourcentage des langues parlees`
FROM country 
JOIN countrylanguage AS cl ON cl.countrycode = country.code 
GROUP BY country.name
ORDER BY `somme des pourcentage des langues parlees`
LIMIT 50;
```

#### Resultat 

| name                                  | somme des pourcentage des langues parlees |
|:--------------------------------------|------------------------------------------:|
| Wallis and Futuna                     |                                       0.0 |
| Cayman Islands                        |                                       0.0 |
| Christmas Island                      |                                       0.0 |
| Tokelau                               |                                       0.0 |
| Cook Islands                          |                                       0.0 |
| Virgin Islands, British               |                                       0.0 |
| Saint Pierre and Miquelon             |                                       0.0 |
| Cocos (Keeling) Islands               |                                       0.0 |
| Saint Helena                          |                                       0.0 |
| Falkland Islands                      |                                       0.0 |
| Pitcairn                              |                                       0.0 |
| Svalbard and Jan Mayen                |                                       0.0 |
| Holy See (Vatican City State)         |                                       0.0 |
| United States Minor Outlying Islands  |                                       0.0 |
| Montserrat                            |                                       0.0 |
| Anguilla                              |                                       0.0 |
| East Timor                            |                                       0.0 |
| Niue                                  |                                       0.0 |
| Norfolk Island                        |                                       0.0 |
| Turks and Caicos Islands              |                                       0.0 |
| Qatar                                 |                                      40.7 |
| United Arab Emirates                  |                                      42.0 |
| Zambia                                |                                      64.9 |
| Bahrain                               |                                      67.7 |
| Côte d’Ivoire                         |                                      71.3 |
| Togo                                  |                                      71.6 |
| Uganda                                |                                      74.0 |
| Burkina Faso                          |                                      74.8 |
| Liberia                               |                                      75.3 |
| Tanzania                              |                                      75.5 |
| Oman                                  |                                      76.7 |
| Malaysia                              |                                      76.8 |
| Kuwait                                |                                      78.1 |
| Mayotte                               |                                      78.3 |
| Gambia                                |                                      79.7 |
| Congo, The Democratic Republic of the |                                      80.3 |
| Mali                                  |                                      80.6 |
| Monaco                                |                                      80.6 |
| Cameroon                              |                                      80.7 |
| Suriname                              |                                      81.0 |
| Gabon                                 |                                      81.3 |
| Indonesia                             |                                      81.9 |
| Central African Republic              |                                      82.4 |
| Ethiopia                              |                                      83.0 |
| Mozambique                            |                                      86.1 |
| Brunei                                |                                      86.7 |
| Nepal                                 |                                      86.8 |
| Sao Tome and Principe                 |                                      87.0 |
| Northern Mariana Islands              |                                      87.3 |
| Guinea-Bissau                         |                                      87.5 |

#### Commentaire

Je n'ai pas d'analyse sur cette question. il semblerait qu'une part de la population ne soit 
pas recense enfin du moins leur langue maternelle suivant l'interpretation que nous avons eu des
doneees.

### Faire une carte du monde, avec une couleur par région. (chaque pays étant dans la bonne couleur).

[map](map.html)