if [ -f .env ]; then
  export $(cat .env | xargs)
fi
if [ -z "${PROJECT_DIR}" ]; then
  export PROJECT_DIR=$(dirname $(realpath "$0"))
  echo "PROJECT_DIR=$PROJECT_DIR" >> .env
fi
if [ ! -d "$PROJECT_DIR"/data ]; then
  mkdir "$PROJECT_DIR"/data
fi

wget https://datahub.io/core/language-codes/r/language-codes.csv -O data/language.csv
wget https://www.mysqltutorial.org/wp-content/uploads/2023/10/mysqlsampledatabase.zip -O data/mysqlsampledatabase.zip && unzip data/mysqlsampledatabase.zip -d data/
wget https://downloads.mysql.com/docs/world-db.tar.gz -O data/world-db.tar.gz && tar -xvf data/world-db.tar.gz -C data/

read -p 'Username with create database auth: ' USER
read -sp 'Password: ' PASS
mv "$PROJECT_DIR"/data/world-db/*.sql "$PROJECT_DIR"/data/
cat "$PROJECT_DIR"/data/*.sql | mysql -u $USER --password=$PASS
mysqldump --no-data classicmodels -u $USER --password=$PASS > "$PROJECT_DIR"/scripts/create_table.sql
mysql -u $USER --password=$PASS --database=classicmodelsv2 < "$PROJECT_DIR"/scripts/init.sql
mysql -u $USER --password=$PASS --database=classicmodelsv2 < "$PROJECT_DIR"/scripts/create_table.sql
mysql -u $USER --password=$PASS --database=classicmodelsv2 < "$PROJECT_DIR"/scripts/migration.sql
mysql -u $USER --password=$PASS --database=classicmodelsv2 < "$PROJECT_DIR"/scripts/privileges.sql
mysql -u $USER --password=$PASS --database=classicmodelsv2 < "$PROJECT_DIR"/scripts/transferdata.sql
python3 -m venv ./venv
source venv/bin/activate
pip install -r "$PROJECT_DIR"/requirements.txt
python3 "$PROJECT_DIR"/scripts/transfer_data_v1_to_v2.py
deactivate
