import plotly.express as px
import pandas as pd
import mysql.connector

cnx = mysql.connector.connect(user='npo', password='F7c262H4;',
                                  host='127.0.0.1',
                                  database='world')
df = pd.read_sql("SELECT country.Code, country.Name, country.Region FROM country",cnx)
print(df)
fig = px.choropleth(df, locations="Code",
                    color="Region", # lifeExp is a column of gapminder
                    hover_name="Name", # column to add to hover information
                    color_continuous_scale=px.colors.sequential.Plasma)
fig.write_html("docs/map.html")
cnx.close()