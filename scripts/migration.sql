ALTER TABLE customers MODIFY country CHAR(3) NOT NULL;
ALTER TABLE offices MODIFY country CHAR(3) NOT NULL;
ALTER TABLE customers ADD COLUMN language CHAR(2) DEFAULT NULL;
ALTER TABLE orders MODIFY status ENUM('Cancelled','Disputed','In Process','On Hold','Resolved','Shipped') DEFAULT 'On Hold' NOT NULL;