from pprint import pprint

import mysql.connector
def format_text_output(d: dict):
    for table in d.keys():
        print(f"## {table}")
        print()
        print(f"### sql :\n```sql\n{d[table]['create_table']["Create Table"]}\n```")
        print()
        print(f"### champs de la table")
        for field in d[table]["fields"]:
            print(f"#### {field.pop("Field")}")
            for k, v in field.items():
                if v:
                    print(f"{k}: {v}", end=', ')
            print()
            print()
        print()
        print()



if __name__ == '__main__':
    session = dict()
    session['cnx'] = mysql.connector.connect(user='npo',
                                             password='F7c262H4;',
                                             host='127.0.0.1',
                                             database='world')
    session['cursor'] = session['cnx'].cursor(dictionary=True)
    session['cursor'].execute('SHOW TABLES;')
    tables = session['cursor'].fetchall()
    data = dict()
    for table in tables:
        data[table["Tables_in_world"]] = dict()
        session['cursor'].execute(f"DESCRIBE {table["Tables_in_world"]};")
        data[table["Tables_in_world"]]["fields"] = session['cursor'].fetchall()
        session["cursor"].execute(f"SHOW CREATE TABLE {table["Tables_in_world"]};")
        data[table["Tables_in_world"]]["create_table"] = session["cursor"].fetchone()
    format_text_output(data)
    session['cursor'].close()
    session['cnx'].close()
