USE world;
DROP TABLE IF EXISTS tmp;
CREATE TABLE tmp(
    Language VARCHAR(50),
    Region VARCHAR(50),
    Pourcent FLOAT
);

INSERT INTO tmp(Language, Region, Pourcent)
SELECT
    LanguageRegion.Language,
    LanguageRegion.Region,
    (LanguageRegion.TotalPopulationSpeakingLanguage*100)/ PopRegion.TotalPopulation AS Pourcent
FROM
    (SELECT
        c.Region,
        SUM(c.Population) AS TotalPopulation
    FROM
        country c
    GROUP BY
        c.Region) AS PopRegion
JOIN
    (SELECT
        cl.Language,
        c.Region,
        SUM((cl.Percentage / 100) * c.Population) AS TotalPopulationSpeakingLanguage
    FROM
        countrylanguage cl
    JOIN
        country c ON cl.CountryCode = c.Code
    GROUP BY
        cl.Language,
        c.Region) AS LanguageRegion
ON PopRegion.Region = LanguageRegion.Region
WHERE PopRegion.TotalPopulation > 0 AND (LanguageRegion.TotalPopulationSpeakingLanguage*100)/ PopRegion.TotalPopulation IS NOT NULL;

SELECT * FROM tmp;
select t.*
from tmp t
left join tmp t2
on t.Region = t2.Region AND t.Pourcent < t2.Pourcent
where t2.Pourcent IS NULL;