import mysql.connector


def language_from_csv(csv_path: str) -> dict:
    result = dict()
    with open(csv_path, "r") as file:
        next(file)
        for line in file:
            code, languages = line.split(',', 1)
            result.update({l.strip(): code for l in languages.split(';')})
    return result


def get_customers_data(cursor) -> list:
    cursor.execute(
        """
        SELECT c.customerNumber, s.language FROM classicmodelsv2.customers AS c
        JOIN (
            SELECT 
                cl.countrycode,
                cl.language,
                ROW_NUMBER() OVER (PARTITION BY cl.countryCode ORDER BY cl.percentage DESC) as rn
            FROM world.countrylanguage AS cl
            WHERE isOfficial = 'T'
        ) as s ON c.country = s.countrycode AND rn = 1
        """
    )
    return cursor.fetchall()


def merge_language(customers: list[dict], languages: dict) -> list[dict]:
    for customer in customers:
        if customer['language'] in languages:
            customer['language'] = languages[customer['language']]
        else:
            print(customer['language'], 'code for this language does not exist')
            customer['language'] = None
    return customers


def to_sql(cursor, connexion, customers):
    for customer in customers:
        cursor.execute(
            """
            UPDATE classicmodelsv2.customers AS c
            SET language = %(language)s
            WHERE c.customerNumber = %(customerNumber)s
            """,
            customer
        )
    connexion.commit()


if __name__ == '__main__':
    cnx = mysql.connector.connect(user='npo',
                                  password='F7c262H4;',
                                  host='127.0.0.1')
    cursor = cnx.cursor(dictionary=True)
    to_sql(
        cursor,
        cnx,
        merge_language(
            get_customers_data(cursor),
            language_from_csv('../data/language.csv')
        )
    )
    cursor.close()
    cnx.close()
